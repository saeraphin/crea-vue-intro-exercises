# Vue.js - exercice 3

Pour créer un composant vide, copier-coller le fichier `EmptyComponent.vue`.

## 1. Créer un composant d'élément de liste

![Vue.js Todo list item screenshot](../00_todo-list-start/src/assets/images/todo-list-item.png?raw=true "Vue.js Todo list - item")

* Créer un composant `TodoListItem` dans le fichier `TodoListItem.vue`
  ``` xml
  <script setup lang="ts">
  </script>

  <template>
  </template>

  <style scoped>
  </style>
  ```

* Rajouter le composant `TodoListItem` dans la boucle de la vue du composant `TodoList`.
* Ce nouveau composant va permettre de revoir la liste et de déplacer la vue des éléments de liste dans un composant dédié.
* Rajouter le markup des éléments de liste et rajouter un nouveau bouton de suppression au composant `TodoListItem`
  ``` html
  <label>
    <input type="checkbox">
    someLabel
  </label>
  <button type="button">Delete</button>
  ```
* La liste devrait afficher les mêmes valeurs pour tous les éléments, il va falloir corriger cela

## 2. Communication parent > enfant

* Créer deux props `label:String` et `done:Boolean` dans la logique du composant du composant `TodoListItem` avec la méthode `defineProps`
* Rajouter la prop `label` dans la vue de l'élément de liste
* Lier la prop `done` à l'attribut `:checked` sur le tag &lt;input&gt;
* Créer une propriété réactive `checked` avec comme valeur par défaut la prop `done`
* Rajouter la propriété `checked` en tant que `v-model` sur le tag &lt;input&gt;
* Dans le composant parent `TodoList`, passer les deux nouvelles propriétés d'entrée aux éléments de la liste
* L'affiche dynamique de la liste devrait être restauré

## 3. Communication enfant > parent

### 3.1 Modification de l'enfant

* Créer deux émetteurs dans la logique du composant du composant `TodoListItem`
  ``` typescript
  const emit = defineEmits<{
    (e: 'toggle', isDone: boolean): void,
    (e: 'delete'): void,
  }>()
  ```

* Lier l'émetteur `toggle` à l'événement `change` du tag &lt;input&gt;.\
  Émettre la valeur de la propriété `done` au travers de l'émetteur de sortie `toggle`
* Lier l'émetteur `delete` au clic sur le bouton de suppression.\
  Faire émettre l'émetteur de sortie `delete` (aucune valeur requise)

### 3.2 Modification du parent

* Lier la méthode `todoStore.updateTodoStatus` à l'événement de sortie `toggle`.\
  Passer l'élément `Todo` ainsi que son nouvel état `done` en paramètres à la méthode du store

* Lier la méthode `todoStore.removeTodo` à l'événement de sortie `delete`\
  Passer l'élément `Todo` en paramètre à la méthode du store

* Le status de complétion d'un élément `TodoItem` est maintenant stocké dans le store `todoStore`
* Il est maintenant possible de supprimer un élément de la liste