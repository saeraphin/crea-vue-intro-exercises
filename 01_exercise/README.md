# Vue.js - exercice 1

Pour créer un composant vide, copier-coller le fichier `EmptyComponent.vue`.

## 1. Créer un composant liste

![Vue.js Todo list screenshot](../00_todo-list-start/src/assets/images/todo-list.png?raw=true "Vue.js Todo list - list")

* Créer un composant vide `TodoList` dans le fichier `TodoList.vue`
  ``` xml
  <script setup lang="ts">
  </script>

  <template>
  </template>

  <style scoped>
  </style>
  ```
* Rajouter le composant `TodoList` dans la vue du composant `Todo` (Remplacer le bloc `List`)\
  `src\components\TodoComponent.vue`
* Rajouter un tableau de *todos* dans la logique du composant `TodoList`

  ``` typescript
  todos: any[] = [
    {id: "1", label: 'Sleep earlier', done: false},
    {id: "2", label: 'Learn JavaScript', done: false},
    {id: "3", label: 'Master Angular', done: false},
    {id: "4", label: 'Enjoy Coding', done: false},
    {id: "5", label: 'Understand Git', done: false},
  ]
  ```
* Créer un fichier `Todo.ts` dans le repertoire `src`.\
  Il va servir à recevoir l'interface `Todo`.
* Rajouter les propriétés des éléments du tableau à l'interface
  ``` typescript
  export interface Todo {
    id: string;
    label: string;
    done: boolean;
  }
  export interface Todos extends Array<Todo> {}
  ```

* Remplacer le type `any` par `Todo[]` ou `Todos` dans la logique du composant `TodoList`

## 2. Lister les tâches

* Créer une liste non ordonnée &lt;ul&gt; de *todos* en utilisant la directive `v-for`

  ``` html
  <li>
    <label>
      <input type="checkbox">
      todo name
    </label>
  </li>
  ```

* Lier le texte affiché dans le &lt;label&gt; à la propriété `label` des éléments *todo*
* Lier l'attribut checked des &lt;input&gt; à la propriété `done` des éléments *todo*
* Lier la propriété d'identification `:key` à la propriété `id` des éléments *todo*
