# Vue.js - exercice 4

Pour créer un composant vide, copier-coller le fichier `EmptyComponent.vue`.

## 1. Créer un formulaire d'édition d'élément de liste

![Vue.js Todo list item form screenshot](../00_todo-list-start/src/assets/images/todo-list-item-form.png?raw=true "Vue.js Todo list - item form")

* Créer un composant `TodoListItemForm` dans le fichier `TodoListItemForm.vue`
  ``` xml
  <script setup lang="ts">
  </script>

  <template>
  </template>

  <style scoped>
  </style>
  ```

* Rajouter le composant `TodoListItemForm` la vue du composant `TodoListItem`
* Ce nouveau composant va permettre d'éditer le label d'un élément TodoItem existant
* Rajouter le markup d'un formulaire avec un champ texte, un bouton annuler et un bouton de soumission
  ``` html
  <form>
    <input name="label" type="text">
    <button type="button">Cancel</button>
    <button type="submit">Confirm</button>
  </form>
  ```
* Le formulaire devrait apparaître dans chaque élément de la liste, il est cependant inerte et encombrant

## 2. Communication parent > enfant

* Créer une prop `label:String` dans la logique du composant du composant `TodoListItemForm` avec la méthode `defineProps`
* Créer une propriété réactive `newLabel` avec comme valeur par défaut la prop `label`
* Rajouter la propriété `newLabel` en tant que `v-model` sur le tag &lt;input&gt;
* Dans le composant parent `TodoListItem`, passer la nouvelle propriété d'entrée aux formulaires de la liste

## 3. Communication enfant > parent

### 3.1 Modification de l'enfant

* Créer deux émetteurs dans la logique du composant du composant `TodoListItemForm`
  ``` typescript
  const emit = defineEmits<{
    (e: 'submit', label: string): void,
    (e: 'cancel'): void,
  }>()
  ```

* Créer une méthode `submit` dans la logique du composant du composant `TodoListItemForm`
* Lier la méthode `submit` à l'événement `submit` du tag &lt;form&gt;.\
  Émettre la valeur de la propriété `label` au travers de l'émetteur de sortie `submit`
* Lier l'émetteur `cancel` au clic sur le bouton d'annulation.\
  Faire émettre l'émetteur de sortie `cancel` (aucune valeur requise)

### 3.2 Modification du parent

* Créer un nouvel émetteur dans la logique du composant du composant `TodoListItem`
  ``` typescript
  const emit = defineEmits<{
    /* ... */
    (e: 'edited', label: string): void,
  }>()
  ```

* Créer une méthode `submit` dans la logique du composant du composant `TodoListItem`
* La méthode doit prendre en paramètre un string
* Lier la méthode `submit` à l'événement de sortie `submit`.\
  Émettre la valeur reçue de l'enfant au travers de l'émetteur de sortie `edited`.\
  Le composant `TodoListItem` ne fait ici que remonter l'événement d'édition à son parent

* Créer une propriété réactive `editing = ref(false)` dans la logique du composant du composant `TodoListItem`
* Créer une méthode `toggleEditing` dans la logique du composant du composant `TodoListItem`
* La méthode doit prendre en paramètre un booléen
* Lier la méthode `toggleEditing` à l'événement de sortie `cancel`.\
  `toggleEditing` doit rendre `isEditing` fausse lors d'un `cancel`

### 3.3 Modification du parent du parent

* Lier la méthode `todoStore.updateTodoLabel` à l'événement de sortie `edited`.\
  Passer l'élément `Todo` ainsi que son nouvel `label` en paramètres à la méthode du store

## 4. Affichage conditionnel du formulaire

* Modifier la vue du composant `TodoListItem`
* Mettre la vue par défaut du composant dans un &lt;template&gt;
* Rendre l'affichage de ces deux nouveaux containers conditionnel avec les directives `v-if` et `v-else`.\
  NB. `v-else` doit être précédé d'un tag utilisant `v-if`
* Rajouter un bouton d'édition à la vue par défaut.\
  Lier le clic du bouton à la méthode `toggleEditing` utilisée précédemment.\
  Cette fois, passer la valeur `true` à la méthode pour activer l'édition.\
  C'est l'événement `cancel` déjà en place de l'enfant qui permettra de sortir du mode d'édition
