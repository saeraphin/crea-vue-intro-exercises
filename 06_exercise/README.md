# Vue.js - exercice 6

Cet exercice aura lieu dans les modules `shared` et `Todo`.

## 1. Création d'un composant container

* Créer un composant `BoxWithTitle` dans le fichier `BoxWithTitle.vue`
  ``` xml
  <script setup lang="ts">
  </script>

  <template>
  </template>

  <style scoped>
  </style>
  ```

* Rajouter le composant `BoxWithTitle` la vue du composant `Todo`.\
  (`src\components\TodoComponent.vue`)
* Ce nouveau composant va consister en un container avec une titre *(quelle surprise)*.\
  Dans le contexte d'une application plus vaste, cela permettrait de centraliser la vue et le style d'un container réutilisable sans avoir à répéter le même code plusieurs fois *(deux fois, c'est une fois de trop)*
* Créer le markup du composant, pour ce faire, extraire le container actuel du composant `Todo` et son titre et les déplacer dans le nouveau composant que nous venons de créer
* Copier-coller le style du composant `Todo` dans le bloc de style du composant `BoxWithTitle`
* Supprimer le bloc de style du composant `Todo`

* Le contenu du composant devrait maintenant (vaguement) apparaître, mais le but recherché n'est pas encore atteint. Ce n'est qu'une coquille vide

## 2. Implémenter la projection de contenu

* L'utilisation du tag &lt;slot&gt; va permettre de déclarer où va s'effectuer le rendu des enfants du container
* Rajouter un premier tag &lt;slot&gt; à la suite du titre dans la vue du composant `BoxWithTitle`
* Dans la vue du composant `Todo`, déplacer les enfants du composant dans le composant `BoxWithTitle`
  ``` html
  <BoxWithTitle>
    <h2>
      <em>Vue.js</em> Todo list
    </h2>
    <TodoForm />
    <div class="notification is-info is-light">Filters</div>
    <div class="notification is-info is-light">Counter</div>
    <TodoList />
  </BoxWithTitle>
  ```

* Les enfants du composant `Todo` devraient maintenant être rendus dans le composant `BoxWithTitle`
* Il reste à déclarer le slot destiné à contenir le titre
* Rajouter un titre &lt;h2&gt; dans la vue du composant `BoxWithTitle`.\
* Rajouter un tag &lt;slot&gt; dans le titre de la vue du composant `BoxWithTitle`.\
  Cette fois, rajouter un attribut name avec pour valeur `title`
* Rajouter un tag &lt;template&gt; avec un attribut `#title` à la vue du composant `Todo`.\
  Y déplacer le contenu du tag &lt;h2&gt;
* Le contenu du tag &lt;template #title&gt; devrait maintenant être projeté dans le container &lt;slot name="title"&gt; lié à cet attribut

## 3. Extra

La solution aux exercices suivants figure dans le répertoire `99_todo-list-final`. L'idée est ici d'essayer d'implémenter deux derniers composants en se basant sur les notions acquises dans la série d'exercices.

### 3.1 Compteur

![Vue.js Todo list counter screenshot](../00_todo-list-start/src/assets/images/todo-counter.png?raw=true "Vue.js Todo list - counter")

* Créer et implémenter le composant `TodoCounter` dans le fichier `TodoCounter.vue`
* Ce composant doit se baser sur l'état du store `todoStore` (il faudra donc l'y injecter)
* Le store `todoStore` expose les tableaux `todos` et `finishedItems`, utiliser les longueurs de ces tableaux pour alimenter le composant
* la logique du composant du composant dans la solution comporte une logique supplémentaire pour faire varier la couleur de la pastille de gauche, vous pouvez y jeter un oeil pour avoir une idée du genre d'amélioration *nice-to-have* que vous pouvez rajouter à des composants quand vous en avez le temps

### 3.2 Filtres

![Vue.js Todo list filters screenshot](../00_todo-list-start/src/assets/images/todo-filters.png?raw=true "Vue.js Todo list - filters")

* Créer et implémenter le composant `TodoFilters` dans le fichier `TodoFilters.vue`
* Ce composant doit se baser sur l'état du store `todoStore` (il faudra donc l'y injecter)
* Le store `todoStore` expose le tableau `filteredItems`, utiliser celui-ci à la place du tableau items dans le composant `TodoList`
* Le store `todoStore` expose la propriété `activeFilter` et la méthode `updateFilter`, utiliser ces deux éléments pour implémenter la logique du composant.\
  Lors de l'utilisation de la méthode `updateFilter`, le tableau retourné par `filteredItems` var être mis à jour en fonction du filtre passé en paramètre
* la logique du composant du composant `TodoList` dans la solution comporte une logique supplémentaire pour afficher différents messages si aucune tâche n'est retournée par le filtre sélectionné, vous pouvez y jeter un oeil pour avoir une idée du genre d'amélioration *nice-to-have* que vous pouvez rajouter à des composants quand vous en avez le temps