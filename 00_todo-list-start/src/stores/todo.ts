import { defineStore } from 'pinia'
import { v4 as uuidv4 } from 'uuid'

export const todoStatuses = ['all', 'unfinished', 'finished']
export type TodoStatus = typeof todoStatuses[number]
export interface Todo { label: string, id: string, done: boolean }
export interface Todos extends Array<Todo> {}

export const useTodoStore = defineStore('todos', {
  state: () => ({
    todos: [] as Todos,
    filter: 'all' as TodoStatus,
  }),
  getters: {
    finishedTodos(state) {
      return state.todos.filter((todo) => todo.done)
    },
    unfinishedTodos(state) {
      return state.todos.filter((todo) => !todo.done)
    },
    filteredTodos(state):Todos {
      switch (state.filter) {
        case 'finished':
          return this.finishedTodos
        case 'unfinished':
          return this.unfinishedTodos
        case 'all':
        default:
          return state.todos
      }
    },
  },
  actions: {
    addTodo(label: string) {
      this.todos = [
        ...this.todos,
        {
          label,
          done: false,
          id: uuidv4(),
        }
      ]
    },
    removeTodo(target: Todo) {
      if (!this.todos.includes(target))
        return
  
      this.todos = this.todos.filter(item => item !== target)
    },
    updateTodoStatus(target: Todo, done: boolean) {
      if (!this.todos.includes(target) || target.done === done)
        return
  
      this.todos = this.todos.map(todo => {
        if (todo === target)
          return { ...todo, done }
        return todo
      })
    },
    updateTodoLabel(target: Todo, label: string) {
      if (!this.todos.includes(target) || target.label === label)
        return
  
      this.todos = this.todos.map(todo => {
        if (todo === target)
          return { ...todo, label }
        return todo
      })
    },
    updateFilter(value: TodoStatus) {
      if (todoStatuses.includes(value))
        this.filter = value
    }
  },
})
