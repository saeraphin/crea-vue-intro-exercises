export const padStart = (text: string, length: number = 2) => {
  const zeros = new Array(length).join('0')
  return (zeros + text).slice(-length)
}

const invert = (hex: string, useBW: boolean) => {
  hex = hex.replace('#', '')

  // convert 3-digit hex to 6-digits.
  if (hex.length === 3)
    hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];

  if (hex.length !== 6)
    throw new Error('Invalid HEX color.')
  
  const r = parseInt(hex.slice(0, 2), 16)
  const g = parseInt(hex.slice(2, 4), 16)
  const b = parseInt(hex.slice(4, 6), 16)

  if (useBW)
    // https://stackoverflow.com/a/3943023/112731
    return (r * 0.299 + g * 0.587 + b * 0.114) > 186
      ? '#000000'
      : '#FFFFFF'

  // invert color components
  const rh = padStart((255 - r).toString(16))
  const gh = padStart((255 - g).toString(16))
  const bh = padStart((255 - b).toString(16))
  // pad each with zeros and return
  return `#${padStart(rh)}${padStart(gh)}${padStart(bh)}`;
}

export default invert
