import { createApp, watch } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'

const app = createApp(App)
const pinia = createPinia()

const STORAGE_KEY = 'vueTodoList'

const storedState = localStorage.getItem(STORAGE_KEY)
if (storedState)
  pinia.state.value = JSON.parse(storedState)

app.use(pinia)
app.mount('#app')

watch(
  pinia.state,
  (state) => {
    // persist the whole state to the local storage whenever it changes
    localStorage.setItem(STORAGE_KEY, JSON.stringify(state))
  },
  { deep: true }
)
