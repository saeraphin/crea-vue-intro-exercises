# Vue.js - exercice 5

## 1. Rajout de l'autofocus sur le formulaire `TodoListItemForm`

* Pour une meilleure expérience utilisateur, il serait intéressant de déplacer le focus sur le champ d'édition lorsque l'on afficher le formulaire d'édition de tâche
* Pour ce faire, un pointeur vers l'élément est requis
  ``` typescript
  const newLabelInput = ref<HTMLInputElement>()
  ```
  Il faut établir le lien avec la propriété réactive en rajoutant l'attribut `ref` au tag &lt;input&gt; avec pour valeur le nom de la propriété réactive
  ``` xml
  <input class="input is-small" type="text" ref="newLabelInput" v-model="newLabel" />
  ```

* La référence à l'élément n'est disponible qu'après l'étape `onMounted` du cycle de vie de Vue.js.\
  Il faut donc se brancher sur cette étape du cycle de vie pour effectuer notre action.
  ``` typescript
  import { ref, onMounted } from 'vue'
  ```

* Dans l'étape `onMounted`, rajouter le focus sur l'élément `newLabelInput` de la vue
  ``` typescript
  onMounted(() => {
    newLabelInput.value && newLabelInput.value.focus()
  })
  ```

## 2. Création d'une directive *autofocus*

* Idéalement, cette logique devrait pouvoir être réutilisable dans d'autres composants
* Créer la directive `AutoFocus` dans le fichier `AutoFocus.dir.vue`
* La syntaxe de la directive est assez restreinte
  ``` xml
  <script lang="ts">
  export const vAutoFocus = {
    mounted: (el:HTMLElement) => el.focus()
  }
  </script>
  ```
* Pour l'utiliser dans un composant, il faut l'importer au préalable
  ``` typescript
  import { vAutoFocus } from './AutoFocus.directive.vue';
  ```
* Il est ensuite possible de l'appliquer à l'élément html désiré
  ``` html
  <input type="text" v-auto-focus>
  ```
* Malheureusement, la documentation officielle de Vue sur les directives custom est lacunaire au possible