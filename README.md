# CREA Vue.js Introduction Exercises

These exercises goal is to create a local storage based todo list from scratch.

They will cover various basic Vue principles and aspects.

![VueJS Todo list screenshot](00_todo-list-start/src/assets/images/todo.png?raw=true "VueJS Todo list")

## Required setup

Go to the `00_todo-list-start` folder and run the install script.

```
$ cd 00_todo-list-start
$ npm install
```

You should then be able to launch a local dev server.

```
$ npm run dev
```

## Folder organisation

Each exercise consists in a series of tasks to be performed. The state you'll reach at the end of each exercise will serve as the starting point of the next exercise.

The changes you will be required to make should only affect the files in the `src` folder.

A `solution` folder is present in each exercise folder, it contains the final state of each exercise. It is recommended not to look at the content of these files straight away.

Should you not be happy with the current state of your work at the end of an exercise, you can back up your current work in a local Git branch and replace the content of the `00_todo-list-start/src` folder with the previous exercise `solution` folder.

The `99_todo-list-final` folder contains the final state of the exercise with a fully functional todo list. It is recommended to only consult this folder at the end of the day.