# Vue.js - exercice 2

Pour créer un composant vide, copier-coller le fichier `EmptyComponent.vue`.

## 1. Créer un composant formulaire

![Vue.js Todo form screenshot](../00_todo-list-start/src/assets/images/todo-form.png?raw=true "Vue.js Todo list - form")

* Créer un composant vide `TodoForm` dans le fichier `TodoForm.vue`
  ``` xml
  <script setup lang="ts">
  </script>

  <template>
  </template>

  <style scoped>
  </style>
  ```

* Rajouter le composant `TodoForm` dans la vue du composant `Todo` (Remplacer le bloc `Item form`)\
  `00_todo-list-start\src\components\TodoComponent.vue`
* Rajouter le markup d'un formulaire simple permettant d'éditer un label et de le soumettre.
  ``` html
  <form>
    <input name="label" type="text" placeholder="Enter todo item name"/>
    <button type="submit">Create</button>
  </form>
  ```
* Créer une propriété réactive `label`, de type `string` dans la logique du composant `TodoForm`
* Utiliser la directive `v-model` sur le tag &lt;input&gt; et lui assigner la propriété `label` nouvellement créée

* Créer une méthode `submit` dans la logique du composant `TodoForm`.\
  Faire un `console.log` de la valeur de la propriété `label` lors de l'appel de la méthode
* Lier la méthode `submit` à l'événement éponyme `submit` du tag &lt;form&gt; dans la vue du formulaire.\
* Lorsque vous soumettez le formulaire en pressant enter ou en cliquant sur le bouton submit, vous devriez voir la valeur actuelle de `label` apparaître dans la console.

## 2. Créer et injecter le store `todoStore`

Ce sujet sera abordé plus tard dans le cours.

* Il est possible d'importer et déclarer le store `todoStore` comme décrit ci-dessous
  ``` typescript
  import { useTodoStore } from '../stores/todo'

  const todoStore = useTodoStore()
  ```

* Nous allons considérer pour le moment ce store comme une boîte noire, l'intellisense de votre IDE devrait vous fournir les méthodes disponibles au sein du store.
* Ce store va permettre de partager un état commun entre différents composants.

## 3. Rajouter un état partagé

* Injecter le store `todoStore` dans la logique du composant de votre composant existant `TodoList`
* Supprimer la propriété `todos` de la logique du composant `TodoList`.\
  Dans la vue, remplacer la propriété `todos` locale par celle du store.
* La liste d'éléments devrait maintenant être vide (c'est normal).

* Injecter le store `todoStore` dans la logique du composant de votre nouveau composant `TodoForm`
* Remplacer le corps de la méthode submit pour effectuer un appel à la méthode `addItem` du store.\
  Réinitialiser la valeur de la propriété `label` après avoir créé un nouvel élément
* Des éléments devraient apparaître dans la liste `TodoList` lorsque le formulaire est soumis