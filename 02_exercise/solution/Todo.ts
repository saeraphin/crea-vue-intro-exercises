export interface Todo {
  id: string;
  label: string;
  done: boolean;
}

export interface Todos extends Array<Todo> {}
